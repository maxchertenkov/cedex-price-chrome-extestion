document.addEventListener("DOMContentLoaded", function() {

	let loading = false;
	const url = 'https://min-api.cryptocompare.com/data/pricemultifull?fsyms=CEDEX&tsyms=USD';
	const refreshBtn = document.getElementById('refresh_button');
	const content = document.getElementById('content');
	const errorMessage = document.getElementById('error_message');
	const loader = document.getElementById('loader');
	const price = document.getElementById('price');
	const percentage = document.getElementById('percentage_24h');

	const getData = url => {
		if (loading) { return; }
		loading = true;
		content.style.display = 'none';
		errorMessage.style.display = 'none';
		loader.style.display = 'block';	
		
		fetch(url)
			.then(response =>  response.json())
			.then(response => {
				loading = false;
				if(response.DISPLAY) {
					processData(response.DISPLAY);
				} else if(response.Response === 'Error') {
					reject(response.Message);
				}
			})
			.catch(error => errorHandler(error) );
	};

	const processData = resposeData => {
		setTimeout(function () {
			loader.style.display = 'none';
			content.style.display = 'block';
			price.innerHTML = resposeData.CEDEX.USD.PRICE;
			percentage.innerHTML = resposeData.CEDEX.USD.CHANGEPCT24HOUR;
		}, 500);
	};

	const errorHandler = error => {
		setTimeout(function () {
			loader.style.display = 'none';
			content.style.display = 'none';
			errorMessage.style.display = 'block';
		}, 500);
		throw new Error(error);
	};

	getData(url);

	refreshBtn.addEventListener( "click", () => {
		getData(url);
	});

});