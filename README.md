# CEDEX Coin price chrome extension

This extension allows to see a popup with current price and percent change of CEDEX Coin.

##How to use:##
1. Download or clone this repository;
1. Go to chrome://extensions/;
1. Toggle "Developer mode";
1. Click on "Load unpacked" button and upload folder with extension;
1. To use just click on extension icon.

